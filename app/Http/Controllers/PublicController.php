<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContattoMail;

class PublicController extends Controller
{
    public function homepage() {
        return view ('welcome');
    }

    public function linkpagina() {
        return view ('linkpag');
    }

    public function submit(Request $req) {
        $user = $req -> input('user');
        $message = $req -> input('message');
        $email = $req -> input('email');

        $contact = compact('user','message');
        // dd($contact);
        Mail::to($email)->send(new ContattoMail($contact));

        return redirect(route('homepage'))->with('message', 'Messagio inviato');
    }
}
