<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{

    public function __construct() 
    {
      $this->middleware('auth');
    }

    public function create() {
        $articles=Article::all();
        return view('blog.create', Compact('articles'));
    }

    public function store(ArticleRequest $req) {
        $article = Article::create([
            'title'=>$req->input('title'),
            'description'=>$req->input('description'),
        ]);

        return redirect(route('blog.create',));
    }

    public function detail(Article $article) {
        return view('blog.detail', compact ('article'));
    }
    
}
