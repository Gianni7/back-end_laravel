<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center margintop">
                <h1>Login</h1>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 offset-md-3">
                    <form method="POST" action="{{route('login')}}">
                        @csrf
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Email</label>
                          <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputPassword1" class="form-label">Password</label>
                          <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <button type="submit" class="btn btn-primary">Invia</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</x-layout>