<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center margintop">
                <h1>Form di Contatti</h1>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-7">
                <h2>Compila il form</h2>
                <form method="POST" action="{{route('contatti.submit')}}">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Nome Utente</label>
                      <input name="user" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Email</label>
                      <input name="email" type="email" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Messaggio</label>
                        <textarea name="message" id="" cols="50" class="form-control" rows="10"></textarea>
                      </div>
                    <button type="submit" class="btn btn-primary">Invia</button>
                  </form>
            </div>
            <div class="col-12 col-md-5">
                <img src="https://picsum.photos/1000" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</x-layout>