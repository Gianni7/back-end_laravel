<x-layout>
    <div class="contanier">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="container margintop">
                    <div class="row">
                        <div class="col-12 col-md-3 mb-4">
                            <div class="card text-center">
                                <img class="card-img-top" src="..." alt="Card image cap">
                                <div class="card-body">
                                <h5 class="card-title">{{$article->title}}</h5>
                                <p class="card-text">{{$article->description}}</p>
                                <a href="{{route('blog.create')}}" class="btn btn-primary">Torna indietro</a>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>