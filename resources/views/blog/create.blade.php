<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center margintop">
                <h1>Blog</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-12 col-md-6 offset-md-3">
                <form method="POST" action="{{route('blog.store')}}">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Titolo</label>
                      <input name="title" type="text" class="form-control" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Descrizione</label>
                      <textarea name="description" type="text" id="" cols="10" rows="10" class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Invia</button>
                  </form>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            @foreach ($articles as $article)
                <div class="col-12 col-md-3 mb-4">
                    <div class="card text-center">
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                        <h5 class="card-title">{{$article->title}}</h5>
                        <p class="card-text">{{$article->description}}</p>
                        <a href="{{route('blog.detail', compact('article'))}}" class="btn btn-primary">Dettaglio</a>
                        </div>
                    </div>
                </div>
            @endforeach   
        </div>
    </div>
</x-layout>