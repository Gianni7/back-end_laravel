<x-layout>
@if (session('message'))
    <div class="alert alert-success mt-5 text-center fs-2">
        {{ session('message') }}
    </div>
@endif
<header>
     <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active img-fluid" style="background-image: url('https://picsum.photos/1920/1080')">
          <div class="carousel-caption">
            <h5>Prima Slide</h5>
          </div>
        </div>
        <div class="carousel-item" style="background-image: url('https://picsum.photos/1920/1081')">
          <div class="carousel-caption">
            <h5>Seconda Slide</h5>
          </div>
        </div>
        <div class="carousel-item" style="background-image: url('https://picsum.photos/1920/1079')">
          <div class="carousel-caption">
            <h5>Terza Slide</h5>
          </div>
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </header>
<div class="container-fluid mt-5 text-center">
    <div class="row lineaalta">
        <div class="col-12 col-md-4 mt-3">
            <h2>Lorem</h2>
            <p class="fs-3">Ipsum 1</p>
        </div>
        <div class="col-12 col-md-4 mt-3">
            <h2>Lorem</h2>
            <p class="fs-3">Ipsum 2</p>
        </div>
        <div class="col-12 col-md-4 mt-3">
            <h2>Lorem</h2>
            <p class="fs-3">Ipsum 3</p>
        </div>
    </div>
</div>
<div class="altezzafinta"></div>
</x-layout>
