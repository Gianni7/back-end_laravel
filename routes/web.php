<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/linkpagina', [PublicController::class, 'linkpagina'])->name('linkpage');
Route::post('/linkpagina/submit', [PublicController::class, 'submit'])->name('contatti.submit');
Route::get('/blog/create', [ArticleController::class, 'create'])->name('blog.create');
Route::post('/blog/store', [ArticleController::class, 'store'])->name('blog.store');
Route::get('/blog/create/detail/{article}', [ArticleController::class, 'detail'])->name('blog.detail');
